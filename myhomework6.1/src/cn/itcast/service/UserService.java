package cn.itcast.service;

import cn.itcast.domain.User;

import java.util.List;

public interface UserService {
    List<User> findAll();

    User login(User user);

    void addUser(User user);


    User findUerById(String id);

    void updateUser(User user);

    void deleteUser(String id);

    void delSelectedUser(String[] ids);
}
