package cn.itcast.dao;

import cn.itcast.domain.User;

import java.util.List;

public interface UserDao {
    List<User> findAll();


    User findUserByUsernameAndPassword(String username, String password);

    void add(User user);

    User findById(int parseInt);

    void update(User user);

    void delete(int parseInt);
}
