package cn.itcast.service;

import cn.itcast.domain.User;

import java.util.List;

public interface UserService {
    public List<User>  findAll();

    User login(User user);

    void addUser(User user);

    void deleteUser(String id);

    User findUserById(String id);

    void updateUser(User user);

    void delSelectedUser(String[] ids);
}
