package it.cast.service;

import it.cast.domain.User;

import java.util.List;

public interface UserService {
    List<User> findAll();


    User findUserById(String id);

    void updateUser(User user);

    void deleteUser(String id);

    void delSelectedUser(String[] uids);
}
