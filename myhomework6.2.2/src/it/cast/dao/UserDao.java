package it.cast.dao;

import it.cast.domain.User;

import java.util.List;

public interface UserDao {
    List<User> findAll();

    void add(User user);

    User findUserByUsernameAndPassword(String username, String password);

    User findUserId(int parseInt);

    void update(User user);


    void delete(int parseInt);
}
