package demo02;
//collection集合的遍历 iterator
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class collection02 {
    public static void main(String[] args) {
//        创建集合对象
        Collection<String> c = new ArrayList<String>();

        c.add("hello");
        c.add("world");
        c.add("java");

        Iterator<String> it = c.iterator();

//        public Iterator<E> iterator() {
//            return new ArrayList.Itr();
//        }

//        private class Itr implements Iterator<E> {
//    }

//        E next 返回迭代中的下一代
//        System.out.println(it.next());
 while (it.hasNext()){
    String s= it.next();
     System.out.println(s);
 }
    }
}