package demo03;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


//创建学生对象，存三个学生
public class ListDemo {
    public static void main(String[] args) {
//        创建list对象
        List<student> list = new ArrayList<student>();

//        创建学生对象
        student s1 = new student("大傻逼", 21);
        student s2 = new student("发", 20);
        student s3 = new student("是的", 30);

//        把学生添加到集合
        list.add(s1);
        list.add(s2);
        list.add(s3);
//        迭代器方式
        Iterator<student> it= list.iterator();
        while (it.hasNext()){
            student s=it.next();
            System.out.println(s.getName()+","+s.getAge());
        }
        System.out.println("-------------------");

        for(int i=0;i<list.size();i++){
           student s= list.get(i);
            System.out.println(s.getName()+","+s.getAge());
        }
   }

}
