package cn.itcast.service;

import cn.itcast.domain.User;

import java.util.List;

public interface UserService {

    List<User> findAll();


    void addUser(User user);

    User findUserById(String id);

    void updateUser(User user);

    void delUser(User user);
}
