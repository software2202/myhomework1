package cn.itcast.dao;

import cn.itcast.domain.User;

import java.util.List;

public interface UserDao {
    List<User> findAll();


    User login(String username, String password);

    void add(User user);
    
    void update(User user);

    User findById(int parseInt);

    void delete(User user);
}
