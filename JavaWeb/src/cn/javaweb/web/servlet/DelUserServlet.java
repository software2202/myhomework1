package cn.javaweb.web.servlet;

import cn.javaweb.service.impl.StudentServiceImpl;
import cn.javaweb.student.Student;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "delUserServlet", value = "/delUserServlet")
public class DelUserServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id");
        StudentServiceImpl service = new StudentServiceImpl();
        service.deleteStu(id);
        response.sendRedirect(request.getContextPath()+"/userListServlet");
    }
}
