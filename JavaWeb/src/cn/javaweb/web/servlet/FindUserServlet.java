package cn.javaweb.web.servlet;

import cn.javaweb.service.impl.StudentServiceImpl;
import cn.javaweb.student.Student;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "findUserServlet", value = "/findUserServlet")
public class FindUserServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id");
        StudentServiceImpl service = new StudentServiceImpl();
       Student student= service.findById(id);
       request.setAttribute("student",student);
       request.getRequestDispatcher("/update.jsp").forward(request,response);
    }
}
