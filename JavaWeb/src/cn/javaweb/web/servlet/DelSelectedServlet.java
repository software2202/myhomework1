package cn.javaweb.web.servlet;

import cn.javaweb.service.StudentService;
import cn.javaweb.service.impl.StudentServiceImpl;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "delSelectedServlet", value = "/delSelectedServlet")
public class DelSelectedServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String[] ids = request.getParameterValues("uid");
        StudentService service = new StudentServiceImpl();
        service.deleteSelected(ids);
        response.sendRedirect(request.getContextPath()+"/userListServlet");
    }
}
