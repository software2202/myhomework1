package cn.javaweb.web.servlet;

import cn.javaweb.dao.impl.StudentDaoImpl;
import cn.javaweb.service.StudentService;
import cn.javaweb.service.impl.StudentServiceImpl;
import cn.javaweb.student.Student;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "userListServlet", value = "/userListServlet")
public class UserListServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        StudentService service = new StudentServiceImpl();
        List<Student> students= service.findAll();
         request.setAttribute("students",students);
        request.getRequestDispatcher("/list.jsp").forward(request,response);
    }
}
