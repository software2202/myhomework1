package cn.javaweb.dao;

import cn.javaweb.student.Student;

import java.util.List;

public interface StudentDao {


    List<Student> fandAll();

    void add(Student student);


    Student findStuByUsernameAndPassword(String username, String password);

    Student findById(int parseInt);


    void update(Student student);

    void delete(int parseInt);
}
