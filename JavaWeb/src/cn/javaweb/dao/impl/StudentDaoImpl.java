package cn.javaweb.dao.impl;

import cn.javaweb.dao.StudentDao;
import cn.javaweb.student.Student;
import cn.javaweb.util.JDBCUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

public class StudentDaoImpl implements StudentDao {
    private JdbcTemplate template=new JdbcTemplate(JDBCUtils.getDataSource());


    @Override
    public List<Student> fandAll() {
      String sql="select * from student";
        List<Student> students = template.query(sql, new BeanPropertyRowMapper<Student>(Student.class));
        return  students;
    }

    @Override
    public void add(Student student) {
        String sql="insert into student values(null,?,?,?,?,?,?,null,null)";
 template.update(sql,student.getName(),student.getSex(),student.getAge(),student.getAddress(),student.getTelephone(),student.getEmail());
    }

    @Override
    public Student findStuByUsernameAndPassword(String username, String password) {
        try{
            String sql="select * from student where  username=? and password=?";
            Student student = template.queryForObject(sql, new BeanPropertyRowMapper<Student>(Student.class),username,password);
            return  student;
        }catch (Exception e){
            e.printStackTrace();
            return  null;
        }
    }

    @Override
    public Student findById(int id) {
       String sql="select *from student where id=?";
     return   template.queryForObject(sql,new BeanPropertyRowMapper<Student>(Student.class),id);
    }

    @Override
    public void update(Student student) {
        String sql="update student set name=?,sex=?,age=?,address=?,telephone=?,email=? where id=?";
        template.update(sql,student.getName(),student.getSex(),student.getAge(),student.getAddress(),student.getTelephone(),student.getEmail(),student.getId());
    }

    @Override
    public void delete(int id) {
        String sql="delete from student where id=?";
        template.update(sql,id);
    }


}
