package cn.javaweb.service;

import cn.javaweb.student.Student;

import java.util.List;

public interface StudentService {


    List<Student> findAll();

    void addStu(Student student);

    Student login(Student student);

    Student findById(String id);

    void update(Student student);

    void deleteStu(String id);

    void deleteSelected(String[] ids);
}
