package cn.javaweb.service.impl;

import cn.javaweb.dao.StudentDao;
import cn.javaweb.dao.impl.StudentDaoImpl;
import cn.javaweb.service.StudentService;
import cn.javaweb.student.Student;

import java.util.List;

public class StudentServiceImpl implements StudentService {
    private StudentDao dao=new StudentDaoImpl();

    @Override
    public List<Student> findAll() {
        return dao.fandAll();
    }

    @Override
    public void addStu(Student student) {
        dao.add(student);
    }

    @Override
    public Student login(Student student) {
        return dao.findStuByUsernameAndPassword(student.getUsername(),student.getPassword());
    }

    @Override
    public Student findById(String id) {
        return dao.findById(Integer.parseInt(id));
    }

    @Override
    public void update(Student student) {
        dao.update(student);
    }

    @Override
    public void deleteStu(String id) {
        dao.delete(Integer.parseInt(id));
    }

    @Override
    public void deleteSelected(String[] ids) {
        if (ids!=null&&ids.length>0){
            for (String id : ids) {
                dao.delete(Integer.parseInt(id));
            }
        }
    }


}
