package it.cast.service;

import it.cast.domain.User;

import java.util.List;

public interface UserService {
    List<User> findAll();

    void addUser(User user);
}
