package it.cast.service.impl;

import it.cast.dao.UserDao;
import it.cast.dao.impl.UserDaoImpl;
import it.cast.domain.User;
import it.cast.service.UserService;

import java.util.List;

public class UserServiceImpl implements UserService {
    private UserDao dao=new UserDaoImpl();
    @Override
    public List<User> findAll() {
        return  dao.findAll();
    }

    @Override
    public void addUser(User user) {
        dao.add(user);
    }
}
