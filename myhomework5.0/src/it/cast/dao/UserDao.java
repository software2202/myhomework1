package it.cast.dao;

import it.cast.domain.User;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

public interface UserDao {

    List<User> findAll();

    void add(User user);
}
