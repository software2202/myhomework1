package cn.itcast.service.impl;

import cn.itcast.dao.UserDao;
import cn.itcast.dao.impl.UserDaoImpl;
import cn.itcast.domain.PageBean;
import cn.itcast.domain.User;
import cn.itcast.service.UserService;

import java.util.List;

public class UserServiceImpl implements UserService {


    private UserDao dao = new UserDaoImpl();

    @Override
    public List<User> findAll() {
        //调用Dao完成查询
        return dao.findAll();
    }

    public User login(User user) {
        return dao.findUserByUsernameAndPassword(user.getUsername(), user.getPassword());
    }

    @Override
    public void addUser(User user) {
        dao.add(user);
    }

    @Override
    public void deleteUser(String id) {
         dao.delete(Integer.parseInt(id));
    }

    @Override
    public void delSelectedUser(String[] ids) {
        if (ids != null&&ids.length>0) {
            for (String id : ids) {
                dao.delete(Integer.parseInt(id));
            }
        }
    }

    @Override
    public PageBean<User> findUserByPage(String _currentPage, String _rows) {
      int currentPage= Integer.parseInt(_currentPage);
      int rows=Integer.parseInt(_rows);

        PageBean<User> pb = new PageBean<>();
    pb.setCurrentPage(currentPage);
    pb.setRows(rows);

    int totalCouont= dao.findTotalCount();
    pb.setTotalCount(totalCouont);
    int start=(currentPage-1)*rows;
    List<User> list=dao.findByPage(start,rows);
    pb.setList(list);

    int totalPage=(totalCouont%rows) == 0 ? (totalCouont/rows):(totalCouont/rows)+1;
    pb.setTotalPage(totalPage);
    return  null;
    }
}
