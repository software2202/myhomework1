package it.cast.service.impl;

import it.cast.dao.UserDao;
import it.cast.dao.impl.UserDaoImpl;
import it.cast.domain.User;
import it.cast.service.UserService;

import java.util.List;

public class UserServiceImpl implements UserService {
    private UserDao dao=new UserDaoImpl();
    @Override
    public List<User> findAll() {
        return dao.findAll();
    }

    @Override
    public User findUserById(String id) {
        return dao.findUserId(Integer.parseInt(id));
    }

    @Override
    public void updateUser(User user) {
        dao.update(user);
    }

    @Override
    public void deleteUser(String id) {
         dao.delete(Integer.parseInt(id));
    }

    @Override
    public void delSelectedUser(String[] uids) {
        if(uids!=null&&uids.length>0){
            for (String uid : uids) {
                dao.delete(Integer.parseInt(uid));
            }
        }
    }

    public void addUser(User user) {
        dao.add(user);
    }

    public User login(User user) {
        return  dao.findUserByUsernameAndPassword(user.getUsername(),user.getPassword());
    }
}
