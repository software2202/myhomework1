package cn.itcast.service;

import java.util.List;

//用户管理业务接口
public interface UserService {
    public List<User> findAll();
}
