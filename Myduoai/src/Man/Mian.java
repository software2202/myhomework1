package Man;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

public class Mian {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        PersonSortable2[] p = new PersonSortable2[n];
        for (int i = 0; i < n; i++) {
            String name = sc.nextLine();
            int age = sc.nextInt();
            p[i] = new PersonSortable2(name, age);
        }
        sc.close();
//        Arrays.sort(p, new NameComparator());
        System.out.println("NameComparator:sort");
        for (int i = 0; i < p.length; i++) {
            System.out.println(p[i].toString());
        }
        Arrays.sort(p, new AgeComparator());
        System.out.println("AgeComparator:sort");
        for (int i = 0; i < p.length; i++) {
            System.out.println(p[i].toString());
        }
    }
}
 class PersonSortable2 {
     String name;
     int age;

     public PersonSortable2(String name, int age) {
         this.name = name;
         this.age = age;
     }

     @Override
     public String toString() {
         return name + "-" + age;
     }
 }

abstract class  NameComparator implements Comparator<PersonSortable2> {
    public int compara(PersonSortable2 p1,PersonSortable2 p2) {
    return p1.name.compareTo(p2.name);
    }
}
class AgeComparator implements Comparator<PersonSortable2>{
    public  int compare(PersonSortable2 p1,PersonSortable2 p2){
      return p1.age-p2.age;
    }
}