package it.cast.web.servlet;

import it.cast.service.UserService;
import it.cast.service.impl.UserServiceImpl;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "delSelectedSerclet", value = "/delSelectedSerclet")
public class DelSelectedSerclet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
  this.doPost(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String[] ids = request.getParameterValues("uid");
        UserService service = new UserServiceImpl();
    service.delSelectedUser(ids);
    response.sendRedirect(request.getContextPath()+"/userListServlet");
    }
}
